package fr.chidix.dummybootapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DummyBootAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(DummyBootAppApplication.class, args);
	}
}
